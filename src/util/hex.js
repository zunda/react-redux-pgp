export const arrayToHex = (array) => {
  return Array.prototype.map.call(
    array,
    e => ("0" + e.toString(16).toUpperCase()).slice(-2)
  ).reduce(
    (acc, v, i) => acc + (i > 0 && i % 2 === 0 ? ' ' : '') + v,
    ''
  )
}
