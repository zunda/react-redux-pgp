import React from 'react';
import { Alert, Button, Modal, FormGroup, FormControl } from 'react-bootstrap';
import { keyStore } from '../reducers/Pgp';
import * as openpgp from 'openpgp';
import * as hex from '../util/hex';

export default class Privkey extends React.Component {
  state = {
    keyId: 'No private key',
    text: '',
    passphrase: '',
    show: false,
    buttonFunc: 'Register'
  }

  handleShow = () => {
    this.setState({ show: true });
  }

  handleClose = () => {
    this.setState({ show: false });
  }

  handleSave = () => {
    this.setState({ show: false });
    this.handleSubmit();
  }

  handlePrivkeyChange = (e) => {
    this.setState({ text: e.target.value });
  }

  handlePassphraseChange = (e) => {
    this.setState({ passphrase: e.target.value });
  }

  handleSubmit = () => {
    openpgp.key.readArmored(this.state.text)
    .then(({ keys }) => {
      if (keys.length > 0) {
        keys[0].decrypt(this.state.passphrase)
        .then(() => {
          keyStore.privkey = keys[0];
          const keyFpArray = keys[0].primaryKey.fingerprint;
          const keyId = hex.arrayToHex(keyFpArray.slice(-8));
          const keyFp = hex.arrayToHex(keyFpArray);
          this.props.storePrivkey(keyId, keyFp);
          this.setState({ keyId: keyId, buttonFunc: 'Update', text: '', passphrase: '' });
        })
        .catch(error => {
          this.props.showAlert(error.message);
        });
      } else {
        this.props.showAlert("Could not parse the armored key.");
      }
    })
    .catch(error => {
      this.props.showAlert(error.message);
    });
  }

  render() {
    const armoredSample = "-----BEGIN PGP PRIVATE KEY BLOCK-----\n  :\n-----END PGP PRIVATE KEY BLOCK-----";
    return (
      <div>
        <span fontFamily="monospace">{this.state.keyId}</span>&nbsp;
        <Button onClick={this.handleShow}>{this.state.buttonFunc}</Button>

        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Private key</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Alert bsStyle="warning">
              Be careful NOT to use your production private key.
              While this app tries not to, malicious sites may try to steal your credentials.
            </Alert>
            <FormGroup controlId="privateKeyForm">
              <FormControl componentClass="textarea"
                placeholder={armoredSample}
                value={this.state.text}
                onChange={this.handlePrivkeyChange}
                style={{fontFamily: "monospace", height: "20em"}}
              />
            </FormGroup>
            <FormGroup controlId="passPhraseForm">
              <FormControl type="password"
                placeholder="Passphrase"
                value={this.state.passphrase}
                onChange={this.handlePassphraseChange}
              />
            </FormGroup>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.handleSave}>Register</Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

