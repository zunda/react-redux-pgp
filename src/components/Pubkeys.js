import React from 'react';
import Pubkey from '../containers/Pubkey';
import { ListGroupItem, Form, FormGroup, FormControl, Button } from 'react-bootstrap';

export default class Pubkeys extends React.Component {
  state = {
    text: ''
  }

  handleKeyDown = (e) => {
    if (e.keyCode === 13) {
      this.handleSubmit(e);
    }
  }

  handleChange = (e) => {
    this.setState({ text: e.target.value });
  }

  handleSubmit = (e) => {
    if (this.state.text.length > 0) {
      const p = this.props.pgp.pubkeyList.find(e => e.keybaseUsername === this.state.text);
      if (p !== undefined) {
        if (!p.active) {
          this.props.activatePubkey(p.id);
          this.setState({ text: '' });
        } else {
          this.props.showAlert("Pubkey of same username exists: " + this.state.text);
        }
      } else {
        this.props.addPubkey(this.state.text);
        this.setState({ text: '' });
      }
    }
    e.preventDefault();
  }

  getValidationState = () => {
    const length = this.state.text.length;
    if (length > 0) return 'success';
    return null;
  }

  render() {
    return (
      <div>
        {this.props.pgp.pubkeyList.filter(p => p.active).map((pubkey) =>
          <Pubkey key={pubkey.id} {...pubkey} />
        )}
        <ListGroupItem>
          <Form inline>
            <FormGroup
              controlId="pubkeyForm"
              validationState={this.getValidationState()}
            >
              <FormControl
                type="text"
                value={this.state.text}
                placeholder="Recipient Keybase username"
                onChange={this.handleChange}
                onKeyDown={this.handleKeyDown}
              />
            </FormGroup>&nbsp;
            <Button type="submit" onClick={this.handleSubmit}>Add</Button>
          </Form>
        </ListGroupItem>
      </div>
    );
  }
}
