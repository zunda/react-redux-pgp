import React from 'react'

const Footer = () => (
  <div>
    react-redux-pgp, a sample PGP app on browser,
    is powered by <a href="https://openpgpjs.org/">OpenPGP.js</a>.
    Fork me on <a href="https://gitlab.com/zunda/react-redux-pgp">GItLab</a>!
  </div>
)

export default Footer
