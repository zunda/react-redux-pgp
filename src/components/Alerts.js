import React from 'react';
import { Alert } from 'react-bootstrap';

export default class Alerts extends React.Component {
  handleSubmit = (i) => {
    this.props.dismissAlert(i);
  }

  render() {
    const alertHash = this.props.alerts.alertHash;
    const ids = Object.keys(alertHash).sort();
    return (
      <div>
        {ids.map(i =>
          <Alert
            bsStyle="danger"
            key={i}
            onDismiss={() => this.handleSubmit(i)}>
            {alertHash[i]}
          </Alert>
        )}
      </div>
    );
  }
}
