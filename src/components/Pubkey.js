import React from 'react';
import axios from 'axios';
import { ListGroupItem, Button } from 'react-bootstrap';
import { keyStore } from '../reducers/Pgp';
import * as openpgp from 'openpgp';
import * as hex from '../util/hex';

export default class Pubkey extends React.Component {
  state = {
    status: '',
  }

  componentDidMount() {
    if (!this.props.keyFp) {
      this.setState({ status: 'Fetching...' });

      axios.get(this.props.url)
      .then(({ data }) => {
        this.setState({ status: 'Reading...' });
        openpgp.key.readArmored(data)
        .then(({ keys }) => {
          const keyFpArray = keys[0].primaryKey.fingerprint;
          const keyId = hex.arrayToHex(keyFpArray.slice(-8));
          const keyFp = hex.arrayToHex(keyFpArray);
          keyStore.pubkeys[keyFp] = keys;
          this.props.storePubkey(this.props.id, keyId, keyFp);
          this.setState({ status: keyId });
        })
        .catch(error => {
          this.props.showAlert(error.message);
          this.props.deactivatePubkey(this.props.id);
        });
      })
      .catch(error => {
        if (error.response) {
          this.props.showAlert("Error in fetching pubkey from " + this.props.url + " : " + error.response.status);
        } else {
          this.props.showAlert("Error in fetching pubkey from " + this.props.url + " : " + error.message);
        };
        this.props.deactivatePubkey(this.props.id);
      });
    } else {
      this.setState({ status: this.props.keyId });
    }
  }

  render() {
    return (
      <ListGroupItem header={this.props.keybaseUsername}>
        <font fontFamily="monospace">{this.state.status}</font>&nbsp;
        <Button onClick={() => this.props.deactivatePubkey(this.props.id)}>Remove</Button>
      </ListGroupItem>
    );
  }
}
