import React from 'react';
import { FormGroup, FormControl } from 'react-bootstrap';

export default class Output extends React.Component {
  handleFocus = e => {
    e.target.select();
  }

  render() {
    return (
      <div>
        <FormGroup controlId="outputTextForm">
          <FormControl componentClass="textarea"
            placeholder="Output"
            value={this.props.output}
            style={{fontFamily: "monospace", height: "10em"}}
            readOnly
            onFocus={this.handleFocus}
          />
        </FormGroup>
      </div>
    );
  }
}
