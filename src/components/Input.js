import React from 'react';
import * as openpgp from 'openpgp';
import { Button, Form, FormGroup, FormControl } from 'react-bootstrap';
import { keyStore } from '../reducers/Pgp';

export default class Input extends React.Component {
  state = {
    text: ''
  }

  handleChange = (e) => {
    this.setState({ text: e.target.value });
  }

  encrypt = (options) => {
    openpgp.encrypt(options)
    .then(cipherText => {
      this.props.setOutput(cipherText.data);
      this.setState({ text: '' });
    })
    .catch(error => {
      this.props.showAlert(error.message);
    });
  }

  handleEncryptSubmit = (e) => {
    if (this.state.text.length > 0) {
      this.encrypt({
        message: openpgp.message.fromText(this.state.text),
        publicKeys:
          this.props.pgp.pubkeyList.filter(k => (k.keyFp && k.active))
          .map(k => keyStore.pubkeys[k.keyFp])
          .reduce((acc, cur) => acc.concat(cur), [])
      });
    }
    e.preventDefault();
  }

  handleSignEncryptSubmit = (e) => {
    if (this.state.text.length > 0) {
      this.encrypt({
        message: openpgp.message.fromText(this.state.text),
        publicKeys:
          this.props.pgp.pubkeyList.filter(k => (k.keyFp && k.active))
          .map(k => keyStore.pubkeys[k.keyFp])
          .reduce((acc, cur) => acc.concat(cur), []),
        privateKeys: [keyStore.privkey]
      });
    }
    e.preventDefault();
  }

  handleDecryptSubmit = (e) => {
    if (this.state.text.length > 0) {
      openpgp.message.readArmored(this.state.text)
      .then(message => {
        openpgp.decrypt({message: message, privateKeys: [keyStore.privkey]})
        .then(plainText => {;
          this.props.setOutput(plainText.data);
          this.setState({ text: '' });
        })
        .catch(error => {
          this.props.showAlert(error.message);
        });
      })
      .catch(error => {
        this.props.showAlert(error.message);
      });
    }
    e.preventDefault();
  }

  render() {
    return (
      <div>
        <Form>
          <FormGroup controlId="inputTextForm">
            <FormControl componentClass="textarea"
              placeholder="Input"
              value={this.state.text}
              onChange={this.handleChange}
              style={{fontFamily: "monospace", height: "10em"}}
            />
          </FormGroup>
        </Form>
        <Form inline>
          <Button
            type="submit"
            onClick={this.handleEncryptSubmit}
            disabled={!this.props.pgp.canEncrypt || this.state.text.length === 0}
          >
            Encrypt
          </Button>{" "}
          <Button
            type="submit"
            onClick={this.handleSignEncryptSubmit}
            disabled={!this.props.pgp.canEncrypt || !this.props.pgp.canDecrypt || this.state.text.length === 0}
          >
            Sign &amp; Encrypt
          </Button>{" "}
          <Button
            type="submit"
            onClick={this.handleDecryptSubmit}
            disabled={!this.props.pgp.canDecrypt || this.state.text.length === 0}
          >
            Decrypt
          </Button>
        </Form>
      </div>
    );
  }
}
