import { createStore as reduxCreateStore, applyMiddleware, combineReducers } from "redux";
import logger from "redux-logger";
import thunk from "redux-thunk";
import { pgpReducer } from "./reducers/Pgp";
import { alertsReducer } from "./reducers/Alerts";

export default function createStore() {
  const store = reduxCreateStore(
    combineReducers({
      pgp: pgpReducer,
      alerts: alertsReducer,
    }),
    applyMiddleware(
      logger,
      thunk,
    )
  );

  return store;
}
