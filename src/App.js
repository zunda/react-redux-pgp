import React, { Component } from 'react';
import { ListGroup, ListGroupItem } from 'react-bootstrap';
import './App.css';
import Alerts from './containers/Alerts';
import Pubkeys from './containers/Pubkeys';
import Privkey from './containers/Privkey';
import Input from './containers/Input';
import Output from './containers/Output';
import Footer from './containers/Footer';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Alerts />
        <ListGroup>
          <Pubkeys />
          <ListGroupItem><Privkey /></ListGroupItem>
          <ListGroupItem><Input /></ListGroupItem>
          <ListGroupItem><Output /></ListGroupItem>
        </ListGroup>
        <Footer />
      </div>
    );
  }
}

export default App;
