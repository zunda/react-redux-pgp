const initialState = {
  alertHash: {}
}

export const alertsReducer = (state = initialState, action) => {
  const newState = Object.assign({}, state);
  switch (action.type) {
    case 'SHOW_ALERT':
      newState.alertHash[action.id] = action.message;
      return newState;
    case 'DISMISS_ALERT':
      delete newState.alertHash[action.id];
      return newState;
    default:
      return state;
  }
};
