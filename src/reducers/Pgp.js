const initialState = {
  pubkeyList: [],
  privkey: {},
  output: '',
  canEncrypt: false,
  canDecrypt: false,
}

export const keyStore = {
  pubkeys: {}
}

export const pgpReducer = (state = initialState, action) => {
  const newState = Object.assign({}, state);
  switch (action.type) {
    case 'ADD_PUBKEY':
      const pubkey = action.pubkey;
      newState.pubkeyList.push(pubkey);
      return newState;
    case 'STORE_PUBKEY':
      const idx = newState.pubkeyList.findIndex(e => e.id === action.pubkey.id);
      newState.pubkeyList[idx].keyId = action.pubkey.keyId;
      newState.pubkeyList[idx].keyFp = action.pubkey.keyFp;
      newState.canEncrypt = true;
      return newState;
    case 'ACTIVATE_PUBKEY':
      newState.pubkeyList[newState.pubkeyList.findIndex(e => e.id === action.id)].active = true;
      newState.canEncrypt = true;
      return newState;
    case 'DEACTIVATE_PUBKEY':
      newState.pubkeyList[newState.pubkeyList.findIndex(e => e.id === action.id)].active = false;
      if (newState.pubkeyList.find(e => e.active) === undefined) {
        newState.canEncrypt = false;
      }
      return newState;
    case 'STORE_PRIBKEY':
      newState.privkey.keyId = action.privkey.keyId;
      newState.privkey.keyFp = action.privkey.keyFp;
      newState.canDecrypt = true;
      return newState;
    case 'SET_OUTPUT':
      newState.output = action.text;
      return newState;
    default:
      return state;
  }
};
