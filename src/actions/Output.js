export const setOutput = (text) => {
  return {
    type: 'SET_OUTPUT',
    text: text
  };
}
