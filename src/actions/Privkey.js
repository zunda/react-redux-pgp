export const storePrivkey = (keyId, keyFp) => {
  return {
    type: 'STORE_PRIBKEY',
    privkey: {
      keyId: keyId,
      keyFp: keyFp
    }
  };
}
