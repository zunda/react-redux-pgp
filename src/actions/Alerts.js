let nextAlertId = 0

export const showAlert = (message) => {
  return {
    type: 'SHOW_ALERT',
    id: nextAlertId++,
    message: message,
  };
}

export const dismissAlert = (id) => {
  return {
    type: 'DISMISS_ALERT',
    id: id,
  };
}
