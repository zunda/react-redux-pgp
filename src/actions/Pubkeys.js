let nextPubkeyId = 0

export const addPubkey = (keybaseUsername) => {
  return {
    type: 'ADD_PUBKEY',
    pubkey: {
      id: nextPubkeyId++,
      keybaseUsername: keybaseUsername,
      url: 'https://keybase.io/' + keybaseUsername + '/pgp_keys.asc',
      active: true
    }
  };
}

export const activatePubkey = (id) => {
  return {
    type: 'ACTIVATE_PUBKEY',
    id: id,
  };
}

export const deactivatePubkey = (id) => {
  return {
    type: 'DEACTIVATE_PUBKEY',
    id: id,
  };
}

export const storePubkey = (id, keyId, keyFp) => {
  return {
    type: 'STORE_PUBKEY',
    pubkey: {
      id: id,
      keyId: keyId,
      keyFp: keyFp
    }
  };
}
