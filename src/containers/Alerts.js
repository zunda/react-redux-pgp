import { connect } from 'react-redux';
import * as actions from '../actions/Alerts';
import Alerts from '../components/Alerts';

const mapStateToProps = state => {
  return {
    alerts: state.alerts,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dismissAlert: (id) => dispatch(actions.dismissAlert(id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Alerts);
