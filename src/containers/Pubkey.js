import { connect } from 'react-redux';
import Pubkey from '../components/Pubkey';
import * as alertActions from '../actions/Alerts';
import * as pubkeysActions from '../actions/Pubkeys';

const mapStateToProps = state => {
  return {
  }
}

const mapDispatchToProps = dispatch => {
  return {
    showAlert: message => dispatch(alertActions.showAlert(message)),
    deactivatePubkey: id => dispatch(pubkeysActions.deactivatePubkey(id)),
    storePubkey: (id, keyId, keyFp) => dispatch(pubkeysActions.storePubkey(id, keyId, keyFp)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Pubkey);
