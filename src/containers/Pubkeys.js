import { connect } from 'react-redux';
import * as actions from '../actions/Pubkeys';
import * as alertActions from '../actions/Alerts';
import Pubkeys from '../components/Pubkeys';

const mapStateToProps = state => {
  return {
    pgp: state.pgp,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addPubkey: pubkey => dispatch(actions.addPubkey(pubkey)),
    activatePubkey: id => dispatch(actions.activatePubkey(id)),
    showAlert: message => dispatch(alertActions.showAlert(message)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Pubkeys);
