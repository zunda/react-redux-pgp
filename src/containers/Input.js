import { connect } from 'react-redux';
import Input from '../components/Input';
import * as alertActions from '../actions/Alerts';
import * as outputActions from '../actions/Output';

const mapStateToProps = state => {
  return {
    pgp: state.pgp,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    showAlert: (message) => dispatch(alertActions.showAlert(message)),
    setOutput: (text) => dispatch(outputActions.setOutput(text))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Input);
