import { connect } from 'react-redux';
import Privkey from '../components/Privkey';
import * as actions from '../actions/Privkey';
import * as alertActions from '../actions/Alerts';

const mapStateToProps = state => {
  return {
    pgp: state.pgp,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    storePrivkey: (keyId, keyFp) => dispatch(actions.storePrivkey(keyId, keyFp)),
    showAlert: message => dispatch(alertActions.showAlert(message)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Privkey);
