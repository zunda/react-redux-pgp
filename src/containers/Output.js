import { connect } from 'react-redux';
import Output from '../components/Output';

const mapStateToProps = state => {
  return {
    output: state.pgp.output,
  }
}

const mapDispatchToProps = dispatch => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Output);
